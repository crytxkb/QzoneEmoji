import requests

urlBefore = 'http://qzonestyle.gtimg.cn/qzone/em/e'
headers = {
"Host": "qzonestyle.gtimg.cn",
"Pragma": "no-cache",
"Cache-Control": "no-cache",
"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36",
"Accept": "image/webp,image/apng,image/*,*/*;q=0.8",
"Accept-Encoding": "gzip, deflate",
"Accept-Language": "en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7"
}
emojiCodeList = tuple()

def getCodeEmoji(start, end, emojiCodeList):
    print('start', start, 'end:', end)
    for i in range(start, end):
        url = urlBefore + emojiCodeList[i] +  '@2x.gif'
        print(url, end='')
        retry = True
        while retry:
            try:
                r = requests.get(url=url, headers=headers)
                print('\t', r.status_code)
                if r.status_code == 200:
                    with open('img/e{}.gif'.format(emojiCodeList[i]), 'wb') as f:
                        f.write(r.content)
                retry = False
            except Exception as e:
                print(e)
                retry = True


# 多进程查找
def multiProcess():
    splitNum = 32
    from multiprocessing import Process, Manager
    splitLen = len(emojiCodeList) // splitNum
    processList = [None for n in range(splitNum)]
    # 分配
    for id in range(splitNum):
        if id != splitNum - 1 :
            processList[id] = Process(
                target=getCodeEmoji, 
                args=(id*splitLen, id*splitLen + splitLen, emojiCodeList)
            )
        else:
            processList[id] = Process(
                target=getCodeEmoji, 
                args=(id*splitLen, len(emojiCodeList), emojiCodeList)
            )
    for process in processList:
        process.start()

    def hasAlive(processList):
        result = False
        for process in processList:
            result = result or process.is_alive()
            if result == True:
                break
        return result

    while hasAlive(processList):
        # 等待进程结束
        pass

if __name__ == '__main__':
    import os 
    pathlist = os.listdir('img/')
    pathlist = list(filter(lambda x: '.gif' in x, pathlist))
    emojiCodeList = tuple(map(lambda x: x[1:].split('.')[0], pathlist))
    multiProcess()