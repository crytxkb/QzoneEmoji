import requests

htmlHead = """ \
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0" />
    <title>
        扣扣空间表情
    </title>
    <style>
        .copyBtn {
            width: 2.5em;
            height: 2.5em;
            padding: 1px;
            background-color: rgba(1, 1, 1, 0);
            border: 1px;
        }

        img {
            width: 100%;
            height: 100%;
        }

        #show {
            display: none;
            background-color: rgba(1, 1, 1, 0.5);
            padding: 2px 8px 2px 8px;
            color: #cccccc;
            font-size: 18px;
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

        iframe {
            position:fixed;
            z-index:9999; 
            text-align:center;
            background-color: #252527; 
            padding: 0;
            border: 0;
            margin: 0;
            width:100%; 
            height:100%; 
            top: 0;
            left: 0;
            color:white;;
        }
    </style>
    <script src="clipboard/clipboard.js"></script>
</head>

<!-- 加载前的等待页面 -->
<script>
    var _LoadingHtml = '<iframe id="loadingIframe" src="loading.html">页面加载中，请等待...</iframe>';
    document.write(_LoadingHtml);//呈现loading效果
    document.onreadystatechange = completeLoading;//监听加载状态改变
    //加载状态为complete时移除loading效果
    function completeLoading() {
        if (document.readyState == "complete") {
            var loadingMask = document.getElementById('loadingIframe');
            loadingMask.parentNode.removeChild(loadingMask);
        }
    }
</script>

<body>
    <div id="show">已复制 :)</div>

"""

htmlTail = """ \
</body>

    <script type="text/javascript">
        function emojiPressed(emojiCode) {
            showMsg = document.getElementById('show');
            showMsg.style.setProperty("display", "block");
            setTimeout(() => {
                showMsg.style.setProperty("display", "none");
            }, 1000);
        }
        var clipboard = new ClipboardJS('.copyBtn');
    </script>
</html>
"""

if __name__ == '__main__':
    import os 
    pathlist = os.listdir('img/')
    pathlist = list(filter(lambda x: '.gif' in x, pathlist))
    emojiCodeList = tuple(map(lambda x: x.split('.')[0], pathlist))
    Finalhtml = htmlHead
    for code in emojiCodeList:
        Finalhtml += """<button class='copyBtn' data-clipboard-text="[em]{}[/em]" onclick="emojiPressed()"><img src="img/{}.gif"></button>\n""".format(code, code)
    Finalhtml += htmlTail
    with open("index.html", "w", encoding='utf-8') as f:
        f.write(Finalhtml)